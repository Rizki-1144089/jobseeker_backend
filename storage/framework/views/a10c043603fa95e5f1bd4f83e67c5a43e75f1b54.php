<?php $__env->startSection('title'); ?>
<h2>All Users</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Approve</th>
                                            <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr >
                                            <td><?php echo e($row->id); ?></td>
                                            <td><?php echo e($row->full_name); ?></td>
                                            <td><?php echo e($row->username); ?></td>
                                            <td><?php echo e($row->email); ?></td>
                                            <td><?php echo e($row->is_approve); ?></td>
                                            <td><?php echo e($row->role_id); ?></td>
                                            <td>
                                                
                                                <!-- <a href="" type="button" class="btn btn-xs btn-primary">View</a> -->
                                                <!-- <button type="button" class="btn btn-xs btn-info">Approve</button> -->
                                                <form action="/<?php echo e($row->id); ?>/approveuser" method="post">
                                                    <!-- <?php echo e(csrf_field()); ?> -->
                                                    <!-- <input type="hidden" name="_method"  value="DELETE"> -->
                                                    
                                                    <!-- <input type="submit" class="btn btn-xs btn-success" name="submit" value="Approve!">  -->
                                                    <button type="submit" class="btn btn-xs btn-success">Approve!</button>                                            
                                                </form>

                                                <form action="<?php echo e(route('user.destroy', ['user'=>$row->id])); ?>" method="post">
                                                    <?php echo e(csrf_field()); ?>

                                                    <input type="hidden" name="_method"  value="DELETE">
                                                    <button type="button" class="btn btn-xs btn-success">Approve</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">                                                
                                                </form>
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </tbody>
                                </table>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>