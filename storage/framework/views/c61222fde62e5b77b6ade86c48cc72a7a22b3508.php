<?php $__env->startSection('title'); ?>
<h2>Job Type</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

				<div class="col-md-8 classified-controls">
					<h3>Detail Student</h3>
					<ul class="list-group">
						<li class="list-group-item">ID: <?php echo e($users->id); ?></li>
						<li class="list-group-item">Full Name: <?php echo e($users->full_name); ?></li>					
						<li class="list-group-item">Username: <?php echo e($users->username); ?></li>
						<li class="list-group-item">Email: <?php echo e($users->email); ?></li>
						<li class="list-group-item">Gender: <?php echo e($users->gender); ?></li>
						<li class="list-group-item">Role: <?php echo e($users->role_id); ?></li>
					</ul>
					<a href="<?php echo e(URL::to('student/all')); ?>" class="btn btn-primary"> Kembali </a>
				</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>