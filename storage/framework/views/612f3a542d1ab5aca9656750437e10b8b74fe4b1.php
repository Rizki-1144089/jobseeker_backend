<?php $__env->startSection('title'); ?>
<h2>Job Type</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

				<div class="col-md-8 classified-controls">
					<h3>Detail Job</h3>
					<ul class="list-group">
						<li class="list-group-item">ID JOB: <?php echo e($jobs->id); ?></li>
						<li class="list-group-item">DESKRIPSI JOB: <?php echo e($jobs->job_description); ?></li>					
						<li class="list-group-item">FASILITAS JOB: <?php echo e($jobs->job_facilities); ?></li>
						<li class="list-group-item">POSISI JOB: <?php echo e($jobs->job_position); ?></li>
					</ul>
					<a href="<?php echo e(URL::to('job/all')); ?>" class="btn btn-primary"> Kembali </a>
				</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>