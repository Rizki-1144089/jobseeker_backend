<?php $__env->startSection('title'); ?>
<h2>Job Type</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Website</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $company; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr >
                                            <td><?php echo e($row->employer_id); ?></td>
                                            <td><?php echo e($row->company_name); ?></td>
                                            <td><?php echo e($row->company_address); ?></td>
                                            <td><?php echo e($row->company_contact); ?></td>
                                            <td><?php echo e($row->company_email); ?></td>
                                            <td><?php echo e($row->company_website); ?></td>
                                            <td>
                                                <form action="" method="post">
                                                <a href="" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    <?php echo e(csrf_field()); ?>

                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form>
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </tbody>
                                </table>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>