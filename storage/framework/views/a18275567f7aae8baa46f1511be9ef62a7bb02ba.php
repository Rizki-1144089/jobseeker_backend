<?php $__env->startSection('title'); ?>
<h2>Job Type</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Description</th>
                                            <th>Facilities</th>
                                            <th>Job Position</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr >
                                            <td><?php echo e($row->id); ?></td>
                                            <td><?php echo e($row->job_description); ?></td>
                                            <td><?php echo e($row->job_facilities); ?></td>
                                            <td><?php echo e($row->job_position); ?></td>
                                            <td>  

                                            <form action="<?php echo e(URL::to('job')); ?>/delete/<?php echo e($row->id); ?>" method="post">
                                                <a href="<?php echo e(URL::to('job')); ?>/<?php echo e($row->id); ?>" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    <?php echo e(csrf_field()); ?>

                                                    <input type="hidden" name="_method"  value="DELETE">
                                            </form>   

                                                <!-- <form action="" method="post">
                                                <a href="user/<?php echo e($row->id); ?>" type="button" class="btn btn-xs btn-primary">View</a> 
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    <?php echo e(csrf_field()); ?>

                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form> -->
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </tbody>
                                </table>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>