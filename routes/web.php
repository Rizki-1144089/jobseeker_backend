<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!!
|
*/

Route::get('/', function () {
    return view('content/index');
});

Route::get('/master', function () {
    return view('layouts/master');
});

// Route::get('/login', function () {
//     return view('/auth/login');
// });

Route::get('/login', 'Auth\LoginController@adminlogin');

// Route::get('/jobtype', function () {
//     return view('/admin/jobtype');
// });

Route::group(['prefix' => 'user'], function () {
	Route::get('all','UserController@alluser');
});

//Student
Route::group(['prefix' => 'student'], function () {
	// Route::resource('users','UserController');
	// Route::resource('user','UserController');
	Route::get('all','UserController@index');
	Route::get('approved','UserController@appusers');
	Route::get('unapproved','UserController@unappusers');
	Route::get('{id?}', 'UserController@studentdetail');
	Route::get('unappuser/{user}','UserController@showapprove');
	Route::put('/{id?}/approveuser','UserController@approveUser');
});

//Employer
Route::group(['prefix' => 'employer'], function () {
	Route::get('all','UserController@AllEmployer');
	Route::get('approved','UserController@AppEmployer');
	Route::get('unapproved','UserController@UnAppEmployer');
	// Route::resource('Login','Auth\LoginController@index');
});

// Company
Route::group(['prefix' => 'company'], function () {
	Route::get('all','CompanyController@AllCompany');
});

// Job
Route::group(['prefix' => 'job'], function () {
	Route::get('all','JobController@AllJob');
	Route::get('{id?}', 'JobController@show');
	Route::delete('delete/{id}', 'JobController@destroy');
});

//Job Type
Route::group(['prefix' => 'jobtype'], function () {
	Route::get('all','JobTypeController@Alljobtype');
	// Route::get('jobtype','JobTypeController@Alljobtype');
});
