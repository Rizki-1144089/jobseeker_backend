@extends('layouts.master')

@section('title')
<h2>Job Type</h2>
@endsection

@section('content')

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($jobtypes as $row)
                                        <tr >
                                            <td>{{ $row->id }}</td>
                                            <td>{{ $row->type_name }}</td>
                                            <td>
                                                <form action="$row->id" method="post">
                                                <a href="" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form>
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>


@endsection