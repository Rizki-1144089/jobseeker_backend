@extends('layouts.master')

@section('title')
<h2>All Users</h2>
@endsection

@section('content')

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Approve</th>
                                            <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $row)
                                        <tr >
                                            <td>{{ $row->id }}</td>
                                            <td>{{ $row->full_name }}</td>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->is_approve }}</td>
                                            <td>{{ $row->role_id }}</td>
                                            <td>
                                                <form action="" method="post">
                                                <a href="{{URL::to('student')}}/{{$row->id}}" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form>
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>


@endsection