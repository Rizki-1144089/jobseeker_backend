@extends('layouts.master')

@section('title')
<h2>Job Type</h2>
@endsection

@section('content')

				<div class="col-md-8 classified-controls">
					<h3>Detail Student</h3>
					<ul class="list-group">
						<li class="list-group-item">ID: {{$users->id}}</li>
						<li class="list-group-item">Full Name: {{$users->full_name}}</li>					
						<li class="list-group-item">Username: {{$users->username}}</li>
						<li class="list-group-item">Email: {{$users->email}}</li>
						<li class="list-group-item">Gender: {{$users->gender}}</li>
						<li class="list-group-item">Role: {{$users->role_id}}</li>
					</ul>
					<a href="{{URL::to('student/all')}}" class="btn btn-primary"> Kembali </a>
				</div>


@endsection