@extends('layouts.master')

@section('title')
<h2>Job Type</h2>
@endsection

@section('content')

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Website</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($company as $row)
                                        <tr >
                                            <td>{{ $row->employer_id }}</td>
                                            <td>{{ $row->company_name }}</td>
                                            <td>{{ $row->company_address }}</td>
                                            <td>{{ $row->company_contact }}</td>
                                            <td>{{ $row->company_email }}</td>
                                            <td>{{ $row->company_website }}</td>
                                            <td>
                                                <form action="" method="post">
                                                <a href="" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form>
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>


@endsection