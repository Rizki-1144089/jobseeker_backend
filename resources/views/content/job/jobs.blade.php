@extends('layouts.master')

@section('title')
<h2>Job Type</h2>
@endsection

@section('content')

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Description</th>
                                            <th>Facilities</th>
                                            <th>Job Position</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($jobs as $row)
                                        <tr >
                                            <td>{{ $row->id }}</td>
                                            <td>{{ $row->job_description }}</td>
                                            <td>{{ $row->job_facilities }}</td>
                                            <td>{{ $row->job_position }}</td>
                                            <td>  

                                            <form action="{{URL::to('job')}}/delete/{{$row->id}}" method="post">
                                                <a href="{{URL::to('job')}}/{{$row->id}}" type="button" class="btn btn-xs btn-primary">View</a>
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method"  value="DELETE">
                                            </form>   

                                                <!-- <form action="" method="post">
                                                <a href="user/{{ $row->id }}" type="button" class="btn btn-xs btn-primary">View</a> 
                                                <button type="button" class="btn btn-xs btn-info">Edit</button>
                                                    <input type="submit" class="btn btn-xs btn-danger" name="submit" value="Delete">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method"  value="DELETE">
                                                </form> -->
                                                <!--<button type="button" class="btn btn-xs btn-danger" >Delete</button>-->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>


@endsection