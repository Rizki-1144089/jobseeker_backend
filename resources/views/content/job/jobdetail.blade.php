@extends('layouts.master')

@section('title')
<h2>Job Type</h2>
@endsection

@section('content')

				<div class="col-md-8 classified-controls">
					<h3>Detail Job</h3>
					<ul class="list-group">
						<li class="list-group-item">ID JOB: {{$jobs->id}}</li>
						<li class="list-group-item">DESKRIPSI JOB: {{$jobs->job_description}}</li>					
						<li class="list-group-item">FASILITAS JOB: {{$jobs->job_facilities}}</li>
						<li class="list-group-item">POSISI JOB: {{$jobs->job_position}}</li>
					</ul>
					<a href="{{URL::to('job/all')}}" class="btn btn-primary"> Kembali </a>
				</div>


@endsection