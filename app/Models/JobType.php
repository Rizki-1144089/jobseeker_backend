<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
  public $table = 'jobtypes';
  protected $guarded =[];

  public function Job()
  {
  	return $this->hasOne('App\Models\Job','jobtype_id');
   // return $this->belongsTo('App\Models\Job','id');
  }
}
