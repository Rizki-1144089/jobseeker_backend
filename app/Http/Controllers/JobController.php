<?php

namespace App\Http\Controllers;

use DB;
use JWTAuth;
use Request as Token;
use Auth;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Repositories\JobRepository;
class JobController extends Controller
{
  private $job;

   public function __construct(JobRepository $job)
   {
     $this->job = $job;
   }

   public function Alljob()
   {
      $jobs = $this->job->getAllWith(['employer.company','jobType','jobQualify.Qualify']);
      return view('content.job.jobs', compact('jobs'));
      // return response()->json(compact('jobs'), 200);
   }

   public function show($id)
    {
      $jobs = Job::findOrFail($id);
      return view('content.job.jobdetail', compact('jobs'));
        // return view('admin.jobdetail', ['job' => Jobs::findOrFail($id)]);
    }

   // public function index()
   // {
   //    $jobs = $this->job->getAllWith(['employer.company','jobType','jobQualify.Qualify']);
   //    return view('/admin/jobs', compact('jobs'));
   //    // return response()->json(compact('jobs'), 200);
   // }

   public function destroy($id)
      {
        $jobs = $this->job->destroy($id);
        // return response()->json(compact('user'), 200);
        return back();
      }

   // ======================================= Front End =================================================
   public function getAlljob()
   {
    // $with = ['employer.company','jobType','jobQualify.Qualify'];
    // $on = DB::select('SELECT COUNT(*) as jumlah FROM jobs GROUP BY jobtype_id');
    // $jobs = $this->job->getAllWithOn($with,$on);
      $jobs = $this->job->getAllWith(['employer.company','jobType','jobQualify.Qualify']);
      return response()->json(compact('jobs'), 200);
   }

   // public function getAlljobWhere()
   // {
   //  $user = token::instance()->id;
   //  $with = ['employer.company','jobTypeTable.jobType','jobQualify.Qualify'];
   //  $where = ['employer_id'=>$user];
   // $jobs = $this->job->getAllWithWhere($with,$where);
   // }

   public function getJobWhere($jobtype_id)
   {  

      $with = ['employer.company','jobType','jobQualify.Qualify'];
      $where = ['jobtype_id'=>$jobtype_id];
      $jobs = $this->job->getAllWithWhere($with,$where);
      // $jobs = $this->job->getWith(['employer.company','jobType','jobQualify.Qualify'],$jobtype_id);
        
      // $jobs = $this->job->getAllWith(['employer.company','jobType','jobQualify.Qualify']);
      return response()->json(compact('jobs'), 200);
   }

   public function getApplyByEmploy($employer_id)
    {  

      $with = ['employer.company','apply','apply.student','jobType'];
      $where = ['employer_id'=>$employer_id];
      $jobs = $this->job->getAllWithWhere($with,$where);
      // $jobs = $this->job->getWith(['employer.company','jobType','jobQualify.Qualify'],$jobtype_id);
        
      // $jobs = $this->job->getAllWith(['employer.company','jobType','jobQualify.Qualify']);
      return response()->json(compact('jobs'), 200);
    }

    public function countJobType()
    {
      $jobs = DB::select('SELECT jobtype_id,type_name, COUNT(*) as jumlah FROM jobs JOIN jobtypes WHERE jobtypes.id=jobs.jobtype_id GROUP BY jobtype_id, type_name' );
      //dd($jobs);
      return response()->json(compact('jobs'), 200);
    }


   public function getJobDetail($job_id)
   {
     //$job = $this->job->getById($job_id);
     $job = $this->job->getWith(['employer.company','jobTypeTable.jobType','jobQualify.Qualify'],$job_id);

     return response()->json(compact('job'), 200);
   }

   public function storeJob(Request $request)
   {
     $id_token = Token::instance()->id;
     $attributes = $request->only('jobtype_id','job_description','job_facilities','job_position');
     $attributes['employer_id'] = $id_token;
     $attributes['job_status'] = 1;
     $job =$this->job->create($attributes);
     return response()->json(compact('job'), 200);
   }

   public function updatejob($job_id, Request $request)
   {//  cek dulu, bnr yg punya job?
     $id_token = Token::instance()->id;
     $job = $this->job->getById($job_id);
     if($id_token == $job->employer_id)
     {
     $attributes = $request->all();
     $job = $this->job->update($job_id, $attributes);
     $job = $this->job->getById($job_id);
     return response()->json(compact('job'), 200);
      }
      else
      {
        return response()->json('bukan yg punya job', 200);
      }
   }

   public function deletejob($job_id)
   {//cek dulu, bnr yg punya job?
      // $user = Auth::user()->id;
     $token = Token::instance()->id;
     // $is_admin = Token::instance()->role;
     $job = $this->job->getById($job_id);
     // dd();
     // if($id_token == $job->employer_id || $is_admin == 3)
      if($job->employer_id == $token)
      {
       $job = $this->job->destroy($job_id);
       return response()->json(compact('job'), 200);
      }
     else
      {
        return response()->json('error', 200);
      }
   }

   public function getJobStudents($job_id)
   {
     $job = $this->job->getById($job_id);

     return response()->json(compact('job'), 200);
   }

   public function getJobApplies($job_id)
   {
      $job = $this->job->getById($job_id);
      $applies = $job->apply()->get();
      return response()->json(compact('applies'), 200);
   }

   public function getEmployerJobs($employer_id)
      {
        // $employer = token::instance()->id;
        $jobs= $this->job->getById($employer_id);
        dd($jobs);
        return response()->json(compact('jobs'), 200);
       }

   // public function getEmployerJobs()
   //    {
   //      $employer_id = token::instance()->id;
   //      $with = ['role','company','job'];
        
   //      $where =[$employer_id];
   //      // $employerJobs = $this->user->getWith(['userProfile','role','company','job'],$employer);
   //      // $employerJobs = $this->user->getAllWithWhere($with,$where);
   //      $employerJobs = $this->user->getById($employer_id)->job()->get();//->with('job');
   //      return response()->json(compact('employerJobs'), 200);
   //     }



}
