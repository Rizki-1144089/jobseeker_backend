<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;
//karena g bisa kalau sama namanya,makanya diganti jadi reques
use Request as token;
use JWTAuth;
use Illuminate\Http\Request;

class UserController extends Controller
{
     private $user;

      public function __construct(UserRepository $user)
      {
        $this->user = $user;
      }
      //Admin
      public function index()
      {
      $role = 2;//student roles
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 1; //approved
      $where =['role_id'=>$role];
      // $users = $this->user->getAll();
      $users = $this->user->getAllWithWhere($with,$where);

        return view('content.student.user', compact('users'));
      }

      public function alluser()
      {
      // $role = 2;//student roles
      // $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      // $is_approve = 1; //approved
      // $where =['role_id'=>$role];
      // // $users = $this->user->getAll();
      // $users = $this->user->getAllWithWhere($with,$where);
      $users = $this->user->getAllWith(['userProfile','role','studentProfile.studentQualify.Qualify']);

        return view('content.user.alluser', compact('users'));
      }

      public function appusers()
      {
      $role = 2;//student role
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 1; //approved
      $where =['role_id'=>$role,'is_approve'=>$is_approve];
      // $users = $this->user->getAll();
      $users = $this->user->getAllWithWhere($with,$where);
      // return response()->json(compact('jobtypes'), 200);
      return view('content.student.appuser', compact('users'));
      }

      public function unappusers()
      {
      $role = 2;//student role
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 0; //approved
      $where =['role_id'=>$role,'is_approve'=>$is_approve];
      // $users = $this->user->getAll();
      $users = $this->user->getAllWithWhere($with,$where);

      // $is_approve=0;
      // $role = 2;
      // $users = User::with('role')->where('is_approve',$is_approve)->get();
      // return response()->json(compact('jobtypes'), 200);
      return view('content.student.unappuser', compact('users'));
      }

      public function studentdetail($id)
      {
      $users = User::findOrFail($id);
      return view('content.student.detail', compact('users'));
        // return view('admin.jobdetail', ['job' => Jobs::findOrFail($id)]);
      }

      public function AllEmployer()
      {
      $role = 1;//student role
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 0; //approved
      $where =['role_id'=>$role];
      // $users = $this->user->getAll();
      $employer = $this->user->getAllWithWhere($with,$where);
      // return response()->json(compact('jobtypes'), 200);
      return view('content.employer.allemployer', compact('employer'));
      }

      public function AppEmployer()
      {
      $role = 1;//student role
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 1; //approved
      $where =['role_id'=>$role,'is_approve'=>$is_approve];
      // $users = $this->user->getAll();
      $employer = $this->user->getAllWithWhere($with,$where);
      // return response()->json(compact('jobtypes'), 200);
      return view('content.employer.appemployer', compact('employer'));
      }

      public function UnAppEmployer()
      {
      $role = 1;//student role
      $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
      $is_approve = 0; //approved
      $where =['role_id'=>$role,'is_approve'=>$is_approve];
      // $users = $this->user->getAll();
      $employer = $this->user->getAllWithWhere($with,$where);
      // return response()->json(compact('jobtypes'), 200);
      return view('content.employer.unappemployer', compact('employer'));
      }

      //==========================================Front End=====================================================
      public function getAllStudent()
      {
        $role = 2;//student role
        $is_approve = 1; //approved
        $with = ['userProfile','role','studentProfile.studentQualify.Qualify'];
        $where =['role_id'=>$role,'is_approve'=>$is_approve];
        $users = $this->user->getAllWithWhere($with,$where);

        //pakai yang di bawah ini ada angka 8 yang entah dari mana, mungkin dari id dari record pertama
        // $users = $this->user->getAllWith(['userProfile','role','studentProfile.studentQualify.Qualify'])
        //                     ->where('role_id',$role)
        //                     ->where('is_approve',$is_approve);
        return response()->json(compact('users'), 200);
       }
       public function getAllEmployer()
       {
         $role = 1; //employer role
         $is_approve = 1; // approved
         $with = ['userProfile','role','company'];
         $where =['role_id'=>$role,'is_approve'=>$is_approve];
         $users = $this->user->getAllWithWhere($with,$where);
        //  $users = $this->user->getAllWith(['userProfile','role','company'])
        //                      ->where('role_id',$role)
        //                      ->where('is_approve',$is_approve);
         return response()->json(compact('users'), 200);
        }

        // public function getUnapprovedUsers()
        // {
        //   $is_approve=0;
        //   $users = User::with('role')->where('is_approve',$is_approve)->get();
        //   return response()->json(compact('users'), 200);
        //  }

      public function getUserDetail($id)
      {
        $with = ['userProfile','role','studentProfile.studentQualify.Qualify','company'];
        $user = $this->user->getWith($with,$id);
        return response()->json(compact('user'), 200);
      }

      public function storeStudent(Request $request)
      {
        $attributes = $request->only('full_name','username','email','password','is_approve','role_id','gender');
        $attributes['role_id']=2;
        $attributes['is_approve']=0;
        $attributes['password']= bcrypt($attributes['password']);
        $user =$this->user->create($attributes);
        return response()->json(compact('user'), 200);
      }
      public function storeEmployer(Request $request)
      {
        $attributes = $request->only('full_name','username','email','password','is_approve','role_id','gender');
        $attributes['role_id']=1;
        $attributes['is_approve']=0;
        $attributes['password']= bcrypt($attributes['password']);
        $user =$this->user->create($attributes);
        return response()->json(compact('user'), 200);
      }

      public function storeAdmin(Request $request)
      {
        $attributes = $request->only('full_name','username','email','password','is_approve','role_id','gender');
        $attributes['role_id']=3;
        $attributes['is_approve']=1;
        $attributes['password']= bcrypt($attributes['password']);
        $user =$this->user->create($attributes);
        return response()->json(compact('user'), 200);
      }
      public function updateuser($id, Request $request)
      {
        $attributes = $request->only('full_name','gender');
        $user = $this->user->update($id, $attributes);
        $user = $this->user->getById($id);

        return response()->json(compact('user'), 200);
      }

      public function destroy($id)
      {
        $user = $this->user->destroy($id);
        // return response()->json(compact('user'), 200);
        return back();
      }

//okk
      public function approveUser($id, Request $request)
      // public function update($id)
      {
        $id = token::instance()->id;
        $attributes['is_approve'] = 1;
         $user = $this->user->update($user_id, $attributes);
         $user = $this->user->getById($user_id);
               
          // $attributes = Users::where('id', $id)->first();
          // $attributes = $request->only('is_approve');
          // $attributes['is_approve']=1;
          // $user =$this->user->update($attributes);
         
        // return response()->json(compact('user'), 200);
          

          return back();
       }

       public function show($id)
       {
          // $user = $this->user->getById($user_id);.
          return back();
       }
      // public function getEmployerJobs($employer_id)
      // {
      //   // $employer = token::instance()->id;
      //   $employerJobs = $this->user->getWith(['userProfile','role','company','job'],$employer_id);
      //   return response()->json(compact('employerJobs'), 200);
      //  }

       public function getEmployerJobs($employer_id)
      {
        // $employer_id = token::instance()->id;
        $with = ['company','job.jobType'];
        
        $where =[$employer_id];
        $employerJobs = $this->user->getWith($with, $employer_id);
        // $employerJobs = $this->user->getAllWithWhere($with,$where);
        // $employerJobs = $this->user->getById($employer_id)->job()->get();//->with('job');
        return response()->json(compact('employerJobs'), 200);
       }


}
